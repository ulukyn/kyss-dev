!define IconProduct "../app/data/icon.ico"

!define Company "Winch Gate"
!define RegistryCat "HKLM"
!define RegistryKey "Software\${Company}\${GenericProduct}"

Name "${GenericProduct}"
Icon "${IconProduct}"
OutFile "${Executable}"

ShowInstDetails hide
SilentInstall silent
ShowUninstDetails hide
SilentUnInstall silent

RequestExecutionLevel user


!define MUI_ICON "${IconProduct}"
!define MUI_UNICON "${IconProduct}"

Section 
  ReadRegStr $0 HKLM Software\NSIS ""
  ReadRegStr $0 "${RegistryCat}" "${RegistryKey}" "${GenericProduct} Install Path"
  SetOutPath "$0"
  Exec "python\python -X pycache_prefix=cache ryztart.py -e $CMDLINE"
SectionEnd
