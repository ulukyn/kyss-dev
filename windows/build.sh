#!/usr/bin/env bash
########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# ###
# This script will generate the installation packages (an installer.exe) for Windows systems from Linux
# ###


if [[ $1 == "32" ]]
then
	MODE=win32
	WIN_MODE=x32
else
	MODE=amd64
	WIN_MODE=x64
fi

source kyss/unix/utils.sh
source kyss/windows/config.sh

APPNAME=$(grep name setup.cfg | head -n 1 | cut -d" " -f 3 )
KYSS=kyss/windows
Package_name=$(capitalize_first ${APPNAME})

mkdir -p build
cd build 

c $Black$OnYellow "-= CREATING PYTHON FOLDER =-"
cn $Cyan "Downloading https://www.python.org/ftp/python/${PYTHON_FULL_VERSION}/python-${PYTHON_FULL_VERSION}-embed-$MODE.zip..."
curl -s https://www.python.org/ftp/python/${PYTHON_FULL_VERSION}/python-${PYTHON_FULL_VERSION}-embed-$MODE.zip -o python.zip
c $Green "OK"

rm -rf python
mkdir python
cd python

cn $Cyan "Unziping python.zip..."
unzip -q ../python.zip
c $Green "OK"

cn $Cyan "Adding python"${PYTHON_VERS}".zip to python${PYTHON_VERS}._pth ..."
echo "python"${PYTHON_VERS}".zip
.

# Uncomment to run site.main() automatically
import site
" > python${PYTHON_VERS}._pth
c $Green "OK"

c $Black$OnYellow "-= INSTALLING PIP =-"

c $Cyan "Installing pip ..."
curl -s https://bootstrap.pypa.io/get-pip.py -o get-pip.py
wine python.exe get-pip.py 2> /dev/null
c $Green "OK"

CURRENT_PATH=$(pwd)
c $Black$OnYellow "-= INSTALLING KYSS =-"
wine python.exe -m pip install https://gitlab.com/ulukyn/kyss/-/archive/main/kyss-main.zip
wine python.exe -m pip install pywin32

cd ..
rm -rf APP
mkdir APP

c $Black$OnYellow "-= CREATING START EXE =-"
makensis -NOCD -DGenericProduct=${Package_name} -DExecutable=${Package_name}.exe ../${KYSS}/nsis/start.nsi
mv ${Package_name}.exe APP

c $Black$OnYellow "-= CREATING INSTALL EXE =-"
mv python APP/python
cp -r ../app APP
cp ../${APPNAME}.py APP
cp ../${KYSS}/bin/zenity.exe APP

cn $Cyan "Downloading DirectX..."
curl -s  https://download.microsoft.com/download/1/7/1/1718CCC4-6315-4D8E-9543-8E28A4E18C4C/dxwebsetup.exe -o dxwebsetup.exe
c $Green "OK"

cn $Cyan "Downloading VC++ Redist..."
curl -s -L https://aka.ms/vs/17/release/vc_redist.${WIN_MODE}.exe -o vcredist.exe

c $Green "OK"

makensis -NOCD -DGenericProduct=${Package_name} -DExecutable=${Package_name} -DDataDir=../${KYSS}/nsis/ ../${KYSS}/nsis/install.nsi

mkdir -p ../dist/
mv ${Package_name}Installer.exe ../dist/${APPNAME}_installer_${WIN_MODE:1}.exe

c $Black$OnGreen "-= DONE ! =-"
