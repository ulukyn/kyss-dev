#!/bin/bash

# ###
# This script will generate the installation packages (a installer.run inside a .zip) for Linux systems
# The only accepted parameter are : x86_64 or i686
# ###

source kyss/linux/config.sh
source kyss/unix/utils.sh

PACKAGE_NAME=$(grep name setup.cfg | head -n 1 | cut -d" " -f 3 )
if [ ! -d build ]
then
	mkdir build
fi

cd build

MODE=x86_64
APPIMAGE_URL="https://github.com/niess/python-appimage/releases/download/python"$PYTHON_VERSION"/python"$PYTHON_FULL_VERSION"-cp"$PYTHON_VERS"-cp"$PYTHON_VERS"-manylinux1_"$MODE".AppImage"
KYSS=../kyss/linux

rm -rf APP
mkdir APP

c $Cyan "Creating new dist package for python$PYTHON_FULL_VERSION..."

### Download Python AppImage (a python version who works in many linux distributions)
if [ ! -f python.AppImage ]
then
	c $Cyan " Downloading App Image.. $APPIMAGE_URL"
	wget $APPIMAGE_URL -O python.AppImage 2> /dev/null
	chmod a+x python.AppImage
fi

c $Cyan " Extracting App Image..."
### Extract python folder
./python.AppImage --appimage-extract > /dev/null
mv squashfs-root APP/python

c $Cyan " Downloading XCB..."
### Some Qt5 Files are missing, we get it from me.ryzom.com
# this tgz has been created after getting all libxcb from /var/lib/flatpak/runtime/org.kde.Platform/x86_64/5.15/<HASH>/files/lib/x86_64-linux-gnu/
wget https://me.ryzom.com/xcb.tgz 2> /dev/null
tar xvzf xcb.tgz  > /dev/null
mv xcb/*  APP/python/usr/lib/
rm -rf xcb*

PYTHONHOME="python/opt/python$PYTHON_VERSION"
PIP="./python/opt/python$PYTHON_VERSION/bin/pip$PYTHON_VERSION"

### Install App stuff
c $Cyan " Installig App Stuff..."
echo "#!/bin/sh" > APP/install
echo "LD_LIBRARY_PATH=./python/usr/lib:. python/AppRun -X pycache_prefix=cache "${PACKAGE_NAME}".py --install 1" >> APP/install
chmod a+x APP/install

rm -rf ../app/.cache/
cp -r ../app APP
cp ../${PACKAGE_NAME}.py APP
cp ${KYSS}/libs/* APP
cp ${KYSS}/files/* APP

cd APP

rm -rf app/__pycache__/
rm -rf app/modules/*/__pycache__/

rm -rf cache/
rm -rf app/cache
rm -rf app/modules/*/cache

### Install Python Modules depedencies
c $Cyan " Pip install..."
$PIP install -U pip > /dev/null
$PIP install --no-warn-script-locatio https://gitlab.com/ulukyn/kyss/-/archive/main/kyss-main.zip > /dev/null
cd ..


if [ -z $(which makeself) ]
then
	c $Red " Please install makeself"
	exit
fi

c $Cyan " Building self extractor..."
makeself APP package.bin "${INSTALLER_NAME}" ./install --install 1 > /dev/null

c $Cyan " Targziping all..."
rm -rf ${PACKAGE_NAME}_installer
rm -f  ${PACKAGE_NAME}_installer_${MODE}.zip
mkdir ${PACKAGE_NAME}_installer
cd ${PACKAGE_NAME}_installer
mv ../package.bin .
Package_name=$(capitalize_first ${PACKAGE_NAME})
cp ../${KYSS}/bin/installer Install\ ${Package_name}
chmod a+x Install\ ${Package_name}

cd ..
tar cvzf ${PACKAGE_NAME}_installer_x86_64.tgz  ${PACKAGE_NAME}_installer


c $Green "all done !"
