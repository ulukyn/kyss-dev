#!/bin/bash

. kyss/unix/utils.sh

getConfig
SIGN_APP_PASS="$(getVar SIGN_APP_PASS)"
SIGN_EMAIL="$(getVar SIGN_EMAIL)"
SIGN_BUNDLE_ID="$(getVar SIGN_BUNDLE_ID)"

KYSS_DIST=../kyss/macos
APP_NAME=Ryztart.app

if [ ! -d build ]
then
	mkdir build
fi

cd build

if [ ! -f ryztart_macos_app_base.zip ]
then
    c $Black$OnYellow "-= Downloading APP base =-"
    curl -k https://me.ryzom.com/ryztart_macos_app_base.zip -o ryztart_macos_app_base.zip
    unzip ryztart_macos_app_base.zip
fi

c $Cyan "-= Creating $APP_NAME =-"
rm -rf $APP_NAME 
mkdir $APP_NAME
mkdir $APP_NAME/Contents
mkdir $APP_NAME/Contents/MacOS
mkdir $APP_NAME/Contents/Resources

c $Cyan "-= Copying Macos Stuff in App =-"
cp $KYSS_DIST/Info.plist $APP_NAME/Contents
cp $KYSS_DIST/appIcon.icns $APP_NAME/Contents/Resources

c $Cyan "-= Copying Python, scripts and libs in APP =-"
cp $KYSS_DIST/Ryztart $APP_NAME/Contents/Macos
chmod a+x $APP_NAME/Contents/Macos/$PACKAGE_NAME
cp -r install/* $APP_NAME/Contents/Macos

c $Cyan "-= Copying Ryztart in App =-"
rm -rf ../app/__pycache__/
rm -rf ../app/modules/__pycache__/
rm -rf ../app/modules/*/__pycache__/
cp -r ../app $APP_NAME/Contents/Resources
cp -r ../ryztart.py $APP_NAME/Contents/Resources

c $Cyan "-= Installing required pip packages =-"
$APP_NAME/Contents/MacOS/python3 -m pip install -r $KYSS_DIST/../requirements.txt
$APP_NAME/Contents/MacOS/python3 -m pip install -r $KYSS_DIST/requirements.txt

c $Cyan "-= Cleaning MacOS folder =-"
rm -rf $APP_NAME/Contents/Macos/bin/

c $Cyan "-= Signing .so and executables =-"
source $KYSS_DIST/code-signing-config.sh
codesign -s "$SIGNING_IDENTITY_APP" -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/MacOS/lib/python3.9/site-packages/PIL/*.so
codesign -s "$SIGNING_IDENTITY_APP" -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/MacOS/lib/python3.9/site-packages/PIL/.dylibs/*.dylib
codesign -s "$SIGNING_IDENTITY_APP" -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/MacOS/lib/python3.9/site-packages/WebKit/*.so
codesign -s "$SIGNING_IDENTITY_APP" -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/MacOS/lib/python3.9/site-packages/Foundation/*.so
codesign -s "$SIGNING_IDENTITY_APP" -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/MacOS/lib/python3.9/site-packages/AppKit/*.so
codesign -s "$SIGNING_IDENTITY_APP" -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/MacOS/lib/python3.9/site-packages/objc/*.so
codesign -s "$SIGNING_IDENTITY_APP" -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/MacOS/lib/python3.9/site-packages/psutil/*.so
codesign -s "$SIGNING_IDENTITY_APP" -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/MacOS/lib/python3.9/site-packages/CoreFoundation/*.so

codesign -s "$SIGNING_IDENTITY_APP" --options=runtime --signature-size 9400 -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/MacOS/python3
codesign -s "$SIGNING_IDENTITY_APP" --options=runtime --signature-size 9400 -f --entitlements $KYSS_DIST/app.entitlements $APP_NAME/Contents/Resources/app/modules/ryzom_installer/RyzomClientPatcher

chmod a+x $APP_NAME/Contents/MacOS/python3 $APP_NAME/Contents/MacOS/Ryztart

c $Cyan "-= Ziping $APP_NAME =-"
zip -yrq Ryztart.zip $APP_NAME

if [ ! -d macos-installer-builder ]
then
    c $Cyan "-= Clone macos-installer-builder =-"
    git clone https://github.com/KosalaHerath/macos-installer-builder
fi

c $Cyan "-= Creating and Signing pkg =-"
mv Ryztart.zip macos-installer-builder/macOS-x64/application/
# deactive signing
rm -f macos-installer-builder/macOS-x64/build-macos-x64.sh
cd macos-installer-builder/
git checkout macOS-x64/build-macos-x64.sh
cd ..
rm macos-installer-builder/macOS-x64/darwin/Resources/*
cp $KYSS_DIST/installer/* macos-installer-builder/macOS-x64/darwin/Resources/
cp $KYSS_DIST/postinstall macos-installer-builder/macOS-x64/darwin/scripts/
sed -i.bak "s/while true; do/FLAG=true;while false; do/g" macos-installer-builder/macOS-x64/build-macos-x64.sh
sed -i.bak "s/read -p /APPLE_DEVELOPER_CERTIFICATE_ID=\"$SIGNING_IDENTITY\";#read -p /g" macos-installer-builder/macOS-x64/build-macos-x64.sh
macos-installer-builder/macOS-x64/build-macos-x64.sh Ryztart 1.0.0
mv macos-installer-builder/macOS-x64/target/pkg-signed/Ryztart-macos-installer-x64-1.0.0.pkg Ryztart.pkg

c $Cyan "-= Notarizing pkg =-"
notarizationRequestID=$(xcrun altool --notarize-app --primary-bundle-id "$SIGN_BUNDLE_ID" -u "$SIGN_EMAIL" -p "$SIGN_APP_PASS" -f Ryztart.pkg | cut -d" " -f3)
c $Cyan "-= Waiting notarization status for $notarizationRequestID =-"
while ! xcrun altool --notarization-info $notarizationRequestID -u "$SIGN_EMAIL" -p "$SIGN_APP_PASS" --output-format xml |grep -q 'success'
do
    echo "."
    sleep 5
done

xcrun stapler staple "Ryztart.pkg"

c $Green "-= Done! =-"
